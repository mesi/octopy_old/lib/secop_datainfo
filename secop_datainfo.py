import re
import math
import json
import sys
from os import path
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), './')))
from lib.errors import secop_errors
FMT_STR = re.compile('^%[.][0-9]+')
NBR_STR = re.compile('^[0-9\.]+')


def from_structure(structure):
    """Returns datainfo class from datainfo part of structure report"""
    if isinstance(structure, dict):
        data_info_type_name = structure.get('type')
        if data_info_type_name is None:
            return None
#            raise secop_errors.BadJSON('datainfo: missing type in: ' + str(structure))
        data_info_class_name = secop_types[data_info_type_name]
        return data_info_class_name(**structure)
    else:
        return None


def suggest(data, parse_from_string=False):
    """Analyzing data to find out how it could fit into SECoP data types and returning the suggested class"""

    if isinstance(data, bool):
        return SecBool()
    if isinstance(data, int):
        return SecInt()
    if isinstance(data, float):
        return SecFloat()
    if isinstance(data, dict):
        members = dict()
        for k, v in data.items():
            members[k] = suggest(v, parse_from_string).structure()
        return SecStruct(members=members)
    if isinstance(data, list):
        return SecArray(members=suggest(data[0], parse_from_string).structure(), maxlen=len(data))
    if isinstance(data, str):
        if not parse_from_string:
            return SecString()
        if data.startswith('+') or data.startswith('-'):
            data = data[1:]
        if NBR_STR.match(data):
            decimals = data.count('.')
            if decimals == 1:
                return SecFloat()
            elif decimals == 0:
                return SecInt()
            else:
                return SecString()
    print('secop_datainfo.suggest yielded no match for: {}'.format(data))
    return SecString()


class DataInfo:
    def __init__(self, **kwargs):
        self.deserialize(kwargs)
        self._value = None
        self.fmtstr = None

    def deserialize(self, conf):
        pass

    def serialize(self):
        ret = {k: v for k, v in self.__dict__.items() if v is not None and not k.startswith('_')}
        if 'members' in ret.keys():
            members = ret.get('members')
            if isinstance(members, list):
                member_list = list()
                for member in members:
                    if isinstance(member, DataInfo):
                        structure = member.structure()
                        member_list.append(structure)
                ret['members'] = member_list
        elif 'argument' in ret.keys():
            ret['argument'] = ret.get('argument').structure()
            ret['result'] = ret.get('result').structure()
        return ret

    def structure(self):
        return self.serialize()

    def __str__(self):
        return str(self.serialize())

    def validate_value(self, val, set_value=False):
        return self.validate(val, set_value)

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        pass

    def validate_parameter(self, conf, param_name, param_type=None, required=True, node_name=None):
        if required and param_name not in conf:
            if node_name:
                raise secop_errors.NoSuchParameterError(f"required parameter '{param_name}' is missing in '{node_name}'")
            else:
                raise secop_errors.NoSuchParameterError(f"required parameter '{param_name}' is missing")
        elif not required and not param_name in conf:
            return None
        param = conf[param_name]
        if not isinstance(param_type, list):
            param_type = [param_type]
        correct_type = True
        for ptype in param_type:
            if isinstance(param, ptype):
                correct_type = True
        if not correct_type:
            raise secop_errors.WrongTypeError(f"parameter '{param_name}' is the wrong type. Got {param.__class__.__name__}, expected {type.__name__}")
        return param

    def validate_fmtstr(self):
        if self.fmtstr and not FMT_STR.search(self.fmtstr):
            raise secop_errors.WrongTypeError(f"parameter 'fmtstr' has an invalid format: '{self.fmtstr}'")


class SecFloat(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'double'
        super(SecFloat, self).__init__(**kwargs)
        self.min = self.validate_parameter(kwargs, param_name='min', param_type=(int, float), required=False)
        self.max = self.validate_parameter(kwargs, param_name='max', param_type=(int, float), required=False)
        self.unit = self.validate_parameter(kwargs, param_name='unit', param_type=str, required=False)
        self.absolute_resolution = self.validate_parameter(kwargs, param_name='absolute_resolution',
                                                           param_type=(int, float), required=False)
        self.relative_resolution = self.validate_parameter(kwargs, param_name='relative_Resolution',
                                                           param_type=(int, float), required=False)
        self.fmtstr = self.validate_parameter(kwargs, param_name='fmtstr', param_type=str, required=False)
        self.validate_fmtstr()

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        if val is None:
            return
        multiplier = 1
        if isinstance(val, str):
            if val.startswith('+'):
                val = val[1:]
                pass
            if val.startswith('-'):
                val = val[1:]
                multiplier = -1
                pass
        try:
            val = float(val) * multiplier
        except ValueError as err:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        if self.max and set_value:
            if val > self.max:
                raise secop_errors.RangeError(str(val) + ' exceeds the maximum limit of ' + str(self.max))
        if self.min and set_value:
            if val < self.min:
                raise secop_errors.RangeError(str(val) + ' under the lower limit of ' + str(self.min))
        return val


class SecInt(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'int'
        super(SecInt, self).__init__(**kwargs)
        if 'min' not in kwargs.keys():
            kwargs['min'] = -pow(2, 24)
        if 'max' not in kwargs.keys():
            kwargs['max'] = pow(2, 24)

        self.min = self.validate_parameter(kwargs, param_name='min', param_type=(int, float), required=True)
        self.max = self.validate_parameter(kwargs, param_name='max', param_type=(int, float), required=True)
        self.unit = self.validate_parameter(kwargs, param_name='unit', param_type=str, required=False)

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        multiplier = 1
        if isinstance(val, str):
            if val.startswith('+'):
                val = val[1:]
                pass
            if val.startswith('-'):
                val = val[1:]
                multiplier = -1
                pass
        try:
            val = int(val) * multiplier
            if self.min and val < self.min and set_value:
                raise secop_errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' + str(self.max))
            if self.max and val > self.max and set_value:
                raise secop_errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' + str(self.max))
        except ValueError as err:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        except TypeError as err:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        return val


class SecScaled(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'scaled'
        super(SecScaled, self).__init__(**kwargs)
        self.scale = self.validate_parameter(kwargs, param_name='scale', param_type=(int, float), required=True)
        self.min = self.validate_parameter(kwargs, param_name='min', param_type=int, required=True)
        self.max = self.validate_parameter(kwargs, param_name='max', param_type=int, required=True)
        self.unit = self.validate_parameter(kwargs, param_name='unit', param_type=str, required=False)
        self.absolute_resolution = self.validate_parameter(kwargs, param_name='absolute_resolution',
                                                           param_type=(int, float), required=False)
        self.relative_resolution = self.validate_parameter(kwargs, param_name='relative_Resolution',
                                                           param_type=(int, float), required=False)
        self.fmtstr = self.validate_parameter(kwargs, param_name='fmtstr', param_type=str, required=False)
        self.validate_fmtstr()

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        try:
            val = int(float(val)*self.scale)
            if self.min and val < self.min and set_value:
                raise secop_errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' +
                    str(self.max))
            if self.max and val > self.max and set_value:
                raise secop_errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' +
                    str(self.max))
        except ValueError as err:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        return val

    def to_string(self, val):
        '{' + self.fmtstr.format(self.validate(val))


class SecBool(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'bool'
        super(SecBool, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        if isinstance(val, str):
            if val == 'false':
                val = False
            if val == 'true':
                val = True
        try:
            val = bool(val)
        except ValueError as err:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        return val


class SecString(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'string'
        super(SecString, self).__init__(**kwargs)
        self.minchars = self.validate_parameter(kwargs, param_name='minchars', param_type=int, required=False)
        self.maxchars = self.validate_parameter(kwargs, param_name='maxchars', param_type=int, required=False)
        self.isUTF8 = self.validate_parameter(kwargs, param_name='isUTF8', param_type=bool, required=False)

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        try:
            ret = str(val)
        except ValueError as err:
            raise secop_errors.WrongTypeError(str(err))
        return ret


class SecArray(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'array'
        self._py_types = (tuple, list)
        members_class = secop_types[self._members_type]
        self._inner_type = members_class(**kwargs['members'])
        super(SecArray, self).__init__(**kwargs)
        self.members = self.validate_parameter(kwargs, param_name='members', param_type=list, required=True)
        self.maxlen = self.validate_parameter(kwargs, param_name='maxlen', param_type=int, required=True)
        self.minlen = self.validate_parameter(kwargs, param_name='minlen', param_type=int, required=False)


    @property
    def members_type(self):
        return self._inner_type


    def add_member(self, member):
        if isinstance(member, type(self._inner_type)):
            self.maxlen += 1
            # self.members.update(member.structure())
        else:
            raise secop_errors.WrongTypeError('For SecArray all members has to be the same type')

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        ret = []
        if isinstance(val, self._py_types):
            if self.minlen and set_value:
                if len(val) < self.minlen and self.minlen:
                    raise secop_errors.RangeError('Array length less than the specified ' + str(self.minlen) +
                                                  ' for this SecArray')
            if len(val) > self.maxlen and self.minlen and set_value:
                raise secop_errors.RangeError('Array length more than the specified ' + str(self.maxlen) +
                                              ' for this SecArray')
            for element in val:
                ret.append(self._inner_type.validate(element))
            return ret
        else:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error')


class SecStruct(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'struct'
        self._inner_types = {}
        self._py_types = dict
        super(SecStruct, self).__init__(**kwargs)
        self.members = self.validate_parameter(kwargs, param_name='members', param_type=dict, required=True)
        for member in self.members:
            sec_type = self.members.get(member)
            if sec_type.get('type') not in secop_types.keys():
                raise ValueError(sec_type.get('type') + ' is not a valid type')
            self._inner_types[member] = {'func': secop_types.get(sec_type.get('type'))}
            self._inner_types.get(member)['inner_type'] = self._inner_types.get(member).get('func')(**sec_type)
        if not self._inner_types:
            print('Struct members has to be defined, members: {}, kwargs{}'.format(self._inner_types, kwargs))
#           raise ValueError('Struct members has to be defined, members: {}, kwargs{}'.format(self._inner_types, kwargs))
        self.optional = self.validate_parameter(kwargs, param_name='optional', param_type=list, required=False)

    def add_member(self, key, member):
        if issubclass(type(member), DataInfo):
            if isinstance(key, str):
                self.members.update({key: member.structure()})
            else:
                raise secop_errors.WrongTypeError('A SecStruct member has to have a key')
        else:
            raise secop_errors.WrongTypeError('Data type has to belong to the DataInfo class')

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        ret = {}
        if isinstance(val, self._py_types):
            for key in val.keys():
                if key in self._inner_types.keys():
                    ret[key] = self._inner_types.get(key).get('inner_type').validate(val.get(key), set_value)
        else:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ')
        return ret


class SecEnum(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'enum'
        self._py_types = dict
        super(SecEnum, self).__init__(**kwargs)
        self.members = self.validate_parameter(kwargs, param_name='members', param_type=dict, required=True)

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        try:
            ret = int(val)
        except ValueError as err:
            raise secop_errors.WrongTypeError(str(err))
        if ret not in self.members.values() and set_value:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: Number ' + str(ret) +
                                              ' is not represented in enum description')
        return ret


class SecTuple(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'tuple'
        self._py_types = (tuple, list)
        self._inner_types = list()
        super(SecTuple, self).__init__(**kwargs)
        self.members = self.validate_parameter(kwargs, param_name='members', param_type=list, required=True)
        for member in self.members:
            member_class = secop_types[member['type']]
            inner_type = member_class(**member)
            if not isinstance(inner_type, DataInfo):
                raise ValueError('SecTuple can only contain Sec data types')
            self._inner_types.append(inner_type)
        self.members = self._inner_types

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        ret = list()
        nbr_of_values_in = len(val)
        if isinstance(val, self._py_types):
            if nbr_of_values_in < len(self.members) and set_value:
                raise secop_errors.RangeError('Tuple length less than the specified ' + str(len(self.members)) +
                                              ' for this SecTuple')
            if nbr_of_values_in > len(self.members) and set_value:
                raise secop_errors.RangeError('Array length more than the specified ' + str(len(self.members)) +
                                              ' for this SecArray')
            nbr = 0
            for element in val:
                ret.append(self._inner_types[nbr].validate(element, set_value))
                nbr += 1
            return ret
        else:
            raise secop_errors.WrongTypeError('secop_data_type ' + self.type + ' validation error')


class SecCommand(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'command'
        super(SecCommand, self).__init__(**kwargs)
        self.argument = from_structure(kwargs.get('argument'))
        self.result = from_structure(kwargs.get('result'))
        # if not self.argument or not self.result:
        #     raise errors.ProtocolErrorError('Not a valid command datainfo structure')

    def validate(self, val, set_value=False):
        """Validates that a value conforms to the type and limits"""
        raise ValueError('not possible to validate a command, only argument or result')


secop_types = {
    'double': SecFloat,
    'int': SecInt,
    'bool': SecBool,
    'scaled': SecScaled,
    'array': SecArray,
    'string': SecString,
    'struct': SecStruct,
    'enum': SecEnum,
    'tuple': SecTuple,
    'command': SecCommand
}



if __name__ == "__main__":
    b = '1234'
    b_ = suggest(b, True)
    print(b_)
    print(b_.validate(b))
    a = 'test'
    print(suggest(a))
    array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    my_array = suggest(array)
    my_array.add_member(b_)
#    my_array.members.update({'type': 'string'})
    print('validated array: ')
    print(my_array.validate(array))
    print(my_array)
    struct = {"int": 45546, "double": 341.65999999983427, "string": "test"}
    my_struct = suggest(struct)
    print(my_struct)
    print(my_struct.validate(struct))

    to_add = suggest(1234)
    print(to_add)
    my_struct.add_member('a new int', to_add)
    print(my_struct)
    advanced = json.loads('{"int":15035,"double":112.8000000000208,"string":"test","array":[1,2,3,4,5,6,15035]}')
    my_advanced = suggest(advanced)
    print(my_advanced)

    sf = SecFloat(min=0.05, max=99.95)
    sf2 = SecFloat()
    print('valid secfloat: {}'.format(sf2.validate(35.6)))
#    si = SecInt(min=0, max=100)
#    sa = SecArray(type='array', members={'type': 'array', 'members': {'type': 'int', 'min_len': 1, 'max_len': 200}, 'max': 7}, max_len=10)
#    int_test_str = '{"type": "array", "members": {"type": "array", "members": {"type": "int", "min": 1, "max": 200}, "max_len":8}, "max_len":7}'
#     tuple_test_str = '{"type":"tuple","members":[{"type":"enum","members":{"DISABLED":0,"IDLE":100,"WARN":200,"UNSTABLE":270,"BUSY":300,"ERROR":400}},{"type":"string"}]}'
#    scaled_test_str = '{"type": "array", "members": {"type": "array", "members": {"type": "scaled", "min": 1, "max": 200, "scale_factor" : 0.1}, "max_len":8}, "max_len":7}'
    enum_test_str = '{"type":"enum","members":{"OFF":0,"ON":1}}'
    struct_test = '{"type": "struct", "members": {"a": {"type" : "int", "min": 1, "max": 200}},"unit":"bananas", "other stuff":"should not be picked upp"}'
#     struct_test = '{"type":"struct","test":"test","members":{"x":{"type":"int","min":1,"max":200},"y":{"type":"array","members":{"type":"array","members":{"type":"scaled","min":0,"max":200,"scale_factor":1},"min_len":2,"max_len":8},"min_len":3,"max_len":7}}}'
    st = SecStruct(**json.loads(struct_test))
#    sa2 = SecArray(**json.loads(int_test_str))
#    ss = SecArray(**json.loads(scaled_test_str))
    se = SecEnum(**json.loads(enum_test_str))
    print('structure with unit:')
    print(st.structure())
    print(se.structure())

#    print('struct: ' + str(secop_pair_array(**st.structure())))

 #   st_structure = st.structure()
#    print(type(st_structure))
 #   print('st structure: ' + str(st))

#    obj_str = json.loads(struct_test)
 #   print('spa struct: ' + json.dumps(secop_pair_array(dict(json.loads(struct_test)))))
#    print(json.dumps(sf.structure()))
#    print(si)
#    print(json.dumps(si.structure()))
#    print(sa)
#    print(sa2)
#    out = sa.validate(json.loads('[[1,201],[2,3],[4],[3,4,5,6.8]]'))
#    print(out)
#    out = ss.validate(json.loads('[[1,201],[2,3],[4],[3,4,5,6.8]]'))
 #   print(ss)
 #   print(json.dumps(ss.structure()))
#    print(ss)
#     double_json = '{"type":"double","min":-100,"max":200,"unit":"C"}'
#     print('+' * 20)
#     sd = from_structure(json.loads(double_json))
#     print('-'*20)
#     print(sd)
#     print(json.dumps(sd.structure()))

    # out = st.validate(json.loads('{"x": 100, "y": [[1,200],[2,3],[4,5],[3,4,5,6.8]]}'))
    # out = sd.validate('34.56')
    # print('validated double ' + str(out))
    # print(st)
    # print(se.validate(0))
    # my_int = SecInt(min=0, max=100)
    # my_int.value = 10
    # print(my_int)
    # print(my_int.validate('55'))
#     print(json.dumps(my_int.structure()))
#
#     stup = SecTuple(**json.loads(tuple_test_str))
#     stup_test = json.loads('[100, "hej hopp"]')
#     print(stup_test)
#     print(stup.validate(stup_test))
#     print(stup)
#     print(st)

    # command_test_string = '{"argument":{"type":"enum","members":{"Off":0,"On":1}},"result":{"type":"enum","members":{"Off":0,"On":1}},"type":"command"}'
    # test = json.loads(command_test_string)
    # print(type(test))
    # ct = from_structure(test)
    # print(ct.argument.validate(1))
    # print(ct)
    # print(json.dumps(str(ct)))
    sb = suggest('24.0', parse_from_string=True)
    print(sb)
    print(sb.validate('-314'))
    test = b'24.0\r\n'
    print(type(test))
    test.rstrip()
    test = test.decode()
    print(test)
    test2 = dict()
    print(test2)
    if test2:
        print('true')
    else:
        print('false')

    print('-'*20)
    print(type(sb))
    print(isinstance(sb, DataInfo))
