# Secop_datainfo

SECoP data type(datainfo) classes.
Handels nested data types.

##From datainfo-key in structure report
Can create an instance from a dict under the datainfo key in the structure report.
The input has to be parsed to a python dict. 
```python3
sec_struct = from_structure({"type": "struct", "members": {"a": {"type": "int", "min": 1, "max": 200}}})
```

##Suggestion
Suggests a SECoP data type from a python type or optionally a string.
The suggest-function has an option to convert strings to a SECoP datatype. One should strive for having numeric values
transported as a json number but if a device is hardcoded in this way this functionality can be useful. 
The validation method shall be compatible.

Example:
```python3
    sec_int = suggest(1234)  #will return a SecInt class instance
    sec_int = suggest("1234", True)  #will return a SecInt class instance
    sec_string = suggest("1234")  #will return a SecString class instance
```

##Validation
Handles validation. 
Accepts data in strings as suggest() maps them.

Example:
```python3
    value = sec_int.validate("1234")  # will return a python int
    value = sec_int.validate(1234)  # will return a python int
```

###Known issues and further development
Error handling not fully implemented.
Logging not implemented.
No warnings for numbers as strings.